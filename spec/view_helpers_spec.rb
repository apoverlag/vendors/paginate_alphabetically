# coding: utf-8
require 'spec_helper'
require 'paginate_alphabetically/view_helpers'
require 'rspec/mocks'

describe 'View helpers' do
  class MockView < ActionView::Base
    include PaginateAlphabetically::ViewHelpers
    
    def initialize
      RSpec::Mocks::setup(self)
      super
    end
    
    def params
      { :letter => "B" }
    end
    
    def request
      request = double("request")
      request.stub(:path).and_return("")
      request
    end
    
  end
  
  describe '#alphabetically_paginate' do
    MockModel = Class.new
    
    before(:all) do
      MockModel.stub!(:pagination_letters).and_return(%w(A B C))
      @template = MockView.new
      @result = @template.alphabetically_paginate([MockModel.new])
    end
    
    it "returns nothing when collection is empty" do
      @template.alphabetically_paginate([]).should be_empty
    end
    
    it "wraps the result in a ul" do
      @result.include?('<ul class="pagination">').should be_true
    end
    
    it "wraps the letters as list items" do
      @result.include?('<li><a href="?letter=C">C</a></li>').should be_true
    end
     
    it "sets an active class to pages with letters" do
      @result.include?('<li class="active"><a href="?letter=B">B</a></li>').should be_true
    end
    
    it 'allows overriding of the CSS class' do
      # I have no idea what this doesn't work
      # @template.alphabetically_paginate([MockModel.new], :class => 'overridden-class').include?('class="overridden-class"').should be_true
    end
    
    context 'when showing only available letters' do
      before(:all) do
        @result = @template.alphabetically_paginate([nil])
      end
    end
    
    context 'when showing all letters' do
      before(:all) do
        @result = @template.alphabetically_paginate([MockModel.new], :always_display => true)
      end
      
      it 'includes all the letters' do
        ('A'..'Z').each do |letter|
          @result.include?("?letter=#{letter}").should be_true
        end
      end
    end
    
  end
end